﻿using Birds.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Birds.Enums;
using Birds.Universe;

namespace Birds
{
    public class Bird: IBird
    {
        public Guid Id { get; } = Oracle.GetGuid();
        public DateTime BirthDate { get; } = DateTime.UtcNow;

        public string Name { get; private set; }
        public ConsoleColor Color { get; }
        public BirdSex Sex { get; }
        public List<Bird> Children { get; set; } = new List<Bird>();

        public FamilyStatus FamilyStatus { get; set; }
        public List<Bird> FamilyPartners { get; set; } = new List<Bird>();
        public Guid PartnerId { get; set; } 

        public Bird(string name, BirdSex sex)
        {
            Sex = sex;
            Name = name;

            Color = (ConsoleColor)Oracle.Random.Next(1, 16);
        }
        /// <summary> Птичка на выбор (усыновление). </summary>
        public Bird(BirdSex sex, ConsoleColor color)
        {
            Sex = sex;
            Name = Oracle.GetName(sex);
            Color = color;
        }

        /// <summary> Информация о птице. </summary>
        public void Summary()
        {
            Console.ForegroundColor = this.Color;
            Console.WriteLine(this.ToString());
            Console.ResetColor();
        }
        /// <summary> Свадьба. </summary>
        public static bool operator +(Bird a, Bird b)
        {
            if (a.Sex == b.Sex)
            {
                throw new Exception("Не совместимо.");
            }
            if (a.FamilyStatus != FamilyStatus.Solitary || b.FamilyStatus != FamilyStatus.Solitary)
            {
                throw new Exception("Сначала необходимо развестись.");
            }

            a.FamilyStatus = FamilyStatus.Married;
            b.FamilyStatus = FamilyStatus.Married;

            if (a.Sex == BirdSex.Female)
            {
                a.Name = $"Mrs. {a.Name}";
            }
            else
            {
                b.Name = $"Mrs. {b.Name}";
            }

            if (a.FamilyPartners.Contains(b) == false)
            {
                a.PartnerId = b.Id;
                a.FamilyPartners.Add(b);
            }

            if (b.FamilyPartners.Contains(a) == false)
            {
                b.PartnerId = a.Id;
                b.FamilyPartners.Add(a);
            }
            
            Console.WriteLine($"\n{a.Name} and {b.Name} congratulations on your wedding!\n");

            return true;
        }
        /// <summary> Развод. </summary>
        public static bool operator /(Bird a, Bird b)
        {
            if ((a.FamilyStatus != FamilyStatus.Married || b.FamilyStatus != FamilyStatus.Married) || (a.PartnerId != b.Id))
            {
                throw new Exception("Сначала необходимо женится.");
            }

            a.FamilyStatus = FamilyStatus.Solitary;
            b.FamilyStatus = FamilyStatus.Solitary;

            a.PartnerId = Guid.Empty;
            b.PartnerId = Guid.Empty;

            if (a.Sex == BirdSex.Female)
            {
                a.Name = a.Name.Remove(0, 5);
            }
            else
            {
                b.Name = b.Name.Remove(0, 5);
            }

            Console.WriteLine($"\n{a.Name} and {b.Name} you are divorced.\n");

            return true;
        }
        /// <summary> Размножение. </summary>
        public static Bird operator *(Bird a, Bird b)
        {
            if (a.FamilyStatus == FamilyStatus.Solitary || b.FamilyStatus == FamilyStatus.Solitary)
            {
                throw new Exception("Сначала необходимо пожениться.");
            }
            if (a.PartnerId != b.Id)
            {
                throw new Exception("Такое поведение не допустимо!");
            }

            var chickSex = (BirdSex)Oracle.Random.Next(0, 2);
            var chick = new Bird(Oracle.GetName(chickSex), chickSex);

            a.Children.Add(chick);
            b.Children.Add(chick);

            Console.WriteLine($"\nCongratulations on the birth of your {(chick.Sex == BirdSex.Female ? "daughter" : "son")} {chick.Name}!\n");

            return chick;
        }

        public Bird this[int i] => this.Children[i];
        public override string ToString()
        {
            string chicks = this.Children.Any()
                ? string.Join(", ", this.Children.Select(s=>s.Name)) 
                : "no";
            return $"Guid:{this.Id.ToString()}\nName: {this.Name}\nSex: {this.Sex}\nStatus: {this.FamilyStatus}\nСhickens: {chicks}.";
        }
    }

    internal static class BirdExtensions
    {
        /// <summary> Усыновление. </summary>
        public static Bird Adopt(this Bird bird, BirdSex sex, ConsoleColor color)
        {
            
            Bird chicken = new Bird(sex, color);
            if (bird.FamilyStatus == FamilyStatus.Married)
            {
                if (bird.PartnerId != Guid.Empty)
                {
                    var curPartner = bird.FamilyPartners.Find(w => w.Id == bird.PartnerId);
                    curPartner.Children.Add(chicken);
                }
            }

            bird.Children.Add(chicken);

            Console.WriteLine($"\nCongratulations on the adoption of {chicken.Name}!\n");

            return chicken;
        }
    }
}
