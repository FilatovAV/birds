﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Birds.Enums
{
    public enum BirdSex
    {
        Female,
        Male
    }
}
