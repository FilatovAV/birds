﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Birds.Interfaces
{
    public interface IAnimal
    {
        Guid Id { get; }
        DateTime BirthDate { get; }
    }
}
