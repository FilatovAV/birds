﻿using Birds.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Birds.Interfaces
{
    public interface IBird: IAnimal, IFamilyPartner
    {
        public string Name { get; }
        public ConsoleColor Color { get; }
        public BirdSex Sex { get; }
        public List<Bird> Children { get; set; }
    }
}
