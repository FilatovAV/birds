﻿using Birds.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Birds.Interfaces
{
    public interface IFamilyPartner
    {
        Guid PartnerId { get; set; }
        FamilyStatus FamilyStatus { get; set; }
        List<Bird> FamilyPartners { get; set; }
    }
}
