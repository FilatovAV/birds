﻿using Birds.Enums;
using Birds.Universe;
using System;

namespace Birds
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new Bird(Oracle.GetName(BirdSex.Female), BirdSex.Female);
            var b = new Bird(Oracle.GetName(BirdSex.Male), BirdSex.Male);
            a.Summary();
            b.Summary();
            var wedding = a + b;
            a.Summary();
            b.Summary();
            var c = a * b;
            a.Summary();
            b.Summary();
            c.Summary();
            var d = a.Adopt(BirdSex.Female, (ConsoleColor)Oracle.Random.Next(1, 16));
            a.Summary();
            b.Summary();
            c.Summary();
            d.Summary();
            var divorce = a / b;
            a.Summary();
            b.Summary();
            c.Summary();
            d.Summary();

            Console.ReadKey();
        }
    }
}
